package au.com.foxsports.commons.jobqueue;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "idx_student", type = "student")
public class Student extends QueueItem {
    private  String name;
    private String grade;

    public Student() {
    }

    public Student(String id,String name,  String grade) {
        super(id);
        this.name = name;
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Student{" + super.getId() +
                "name='" + name + '\'' +
                ", grade='" + grade + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
