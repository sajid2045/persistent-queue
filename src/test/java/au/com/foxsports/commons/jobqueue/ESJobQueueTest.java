package au.com.foxsports.commons.jobqueue;

import au.com.foxsports.commons.jobqueue.impl.elasticsearch.ESJobQueue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by smoinuddin on 22/03/14.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/applicationContext.xml")

public class ESJobQueueTest {

    private ESJobQueue<Student> jobQueue;
    String indexName = "idx_student";
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;


    @Autowired
    public void setJobQueue(ESJobQueue jobQueue) {
        this.jobQueue = jobQueue;
    }

    @Before
    public void setUp() {
         assert jobQueue != null;
        System.out.println("DELETING INDEX");
        deleteOldData();
    }

    @After
    public void tearDown() {
        //deleteOldData();
    }

    private void deleteOldData() {
        elasticsearchTemplate.deleteIndex(Student.class) ;
    }

    @Test
    public void testElasticsearchUpdateWithVersionBehavior() {
        deleteOldData();;
        Student student = new  Student("11","Sajid", "3.6");

        IndexQuery indexQuery = new IndexQuery();
        indexQuery.setId(student.getId());

        indexQuery.setIndexName(indexName);
        indexQuery.setType("T2");
        indexQuery.setObject(student);
        indexQuery.setVersion(0L);

        elasticsearchTemplate.index(indexQuery);
        elasticsearchTemplate.refresh(indexName, true);

        indexQuery.setObject(student);
        indexQuery.setVersion(1L);

        elasticsearchTemplate.index(indexQuery);
        elasticsearchTemplate.refresh(indexName, true);

        try {
            indexQuery.setObject(student);
            indexQuery.setVersion(0L);
            elasticsearchTemplate.index(indexQuery);
            fail("saved without any problem!!");
        } catch (org.elasticsearch.index.engine.VersionConflictEngineException e) {
             System.out.println("ERROR: " + e.getClass());
        }
    }


    @Test
    public void testContains() {
        deleteOldData();
        jobQueue.submit(new Student("1","",""));
        assertTrue(jobQueue.contains("1"));
    }


    @Test
    public void testTake() {

        deleteOldData();
        for(int i = 1; i <= 10; i++) {
            String id = String.valueOf(i);
            jobQueue.submit(new Student(id, "Sajid: " + id, "GA" + id));
        }

        assertEquals(10, jobQueue.size(QueueItem.Status.QUEUED));
        List<Student> take = jobQueue.take(3);
        System.out.println("take = " + take);
        assertEquals(3, take.size());

        assertEquals(3, jobQueue.size(QueueItem.Status.PROCESSING));
        assertEquals(7, jobQueue.size(QueueItem.Status.QUEUED));


        //make sure first in first out
        for(int i = 1; i <= 3; i++) {
            assertEquals(String.valueOf(i), take.get(i-1).getId());
        }


        jobQueue.markAsDone(take);

        Student take2 = jobQueue.takeOne();

        assertEquals(String.valueOf(4), take2.getId());

        assertEquals(1, jobQueue.size(QueueItem.Status.PROCESSING));

        assertEquals(3, jobQueue.size(QueueItem.Status.DONE));

        assertEquals(6, jobQueue.size(QueueItem.Status.QUEUED));

        jobQueue.markAsDone(take2);

        assertEquals(4, jobQueue.size(QueueItem.Status.DONE));
    }






        @Test
        public  void testCRUD() {

            deleteOldData();
            System.out.println("--- start!");
            Student student = new Student("1","Sajid","4.0");
            jobQueue.submit( student );


            long size = jobQueue.size(QueueItem.Status.QUEUED);
            System.out.println("size = " + size);
            assertEquals(1, size);


            Student queueObject = jobQueue.get("1");

            jobQueue.markAsDone(student);

            assertEquals(0, jobQueue.size(QueueItem.Status.QUEUED));

            assertEquals(1, jobQueue.size(QueueItem.Status.DONE));
        }
}
