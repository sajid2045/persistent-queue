package au.com.foxsports.commons.jobqueue;

import java.util.List;

/**
 * Created by smoinuddin on 22/03/14.
 */
public interface JobQueue<T extends QueueItem> {


    public void submit( T t);

    public boolean contains(String id);

    public long size(QueueItem.Status status);

    public T takeOne();

    public List<T> take(int limit);

    public void markAsDone(T t);
    public void markAsDone(List<T> t);
}
