package au.com.foxsports.commons.jobqueue.impl.elasticsearch;


import au.com.foxsports.commons.jobqueue.JobQueue;
import au.com.foxsports.commons.jobqueue.QueueItem;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.GetQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;


import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Created by smoinuddin on 22/03/14.
 */
public  class ESJobQueue<T extends QueueItem> implements JobQueue<T> {

    private ElasticsearchTemplate elasticsearchTemplate;

    private Class<T> tClass;

    public void submit(T t) {
        save(t);
    }

    private void save(T t) throws org.elasticsearch.index.engine.VersionConflictEngineException {
        IndexQuery indexQuery = new IndexQuery();
        indexQuery.setId(t.getId());
        indexQuery.setObject(t);
        t.setVersion(t.getVersion() + 1);
        t.setLastModified(DateTime.now());
        indexQuery.setVersion(t.getVersion());
        elasticsearchTemplate.index(indexQuery);
        elasticsearchTemplate.refresh(tClass,true);
    }


    public T get(String id) {
        GetQuery query = new GetQuery();
        query.setId(id);
        return elasticsearchTemplate.queryForObject(query, tClass);
    }

    public boolean contains(String id) {
        SearchQuery query =     new NativeSearchQueryBuilder().withQuery(matchQuery("id",id)).build();
        return elasticsearchTemplate.count(query, tClass ) == 1;
    }

    @Override
    public long size(QueueItem.Status status) {
        SearchQuery query =     new NativeSearchQueryBuilder().withQuery(matchQuery("status",status.toString())).build();
        return elasticsearchTemplate.count(query, tClass );
    }

    @Override
    public T takeOne() {
        List<T> take = take(1);
        return CollectionUtils.isEmpty(take) ? null : take.get(0);
    }

    @Override
    public List<T> take(int limit) {
        SearchQuery query =     new NativeSearchQueryBuilder()
                .withQuery(matchQuery("status", QueueItem.Status.QUEUED.toString()))
                .withPageable(new PageRequest(0, limit, new Sort(new Sort.Order(Sort.Direction.ASC,"created"))))
                .build();
        Page<T> pages = elasticsearchTemplate.queryForPage(query, tClass);
        List<T> toReturn = new ArrayList<T>();
        List<T> content = pages.getContent();

        if(content != null && content.size() > 0) {
              for(T c : content) {
                  try {
                      c.setStatus(QueueItem.Status.PROCESSING);
                      save(c);
                      toReturn.add(c);
                  } catch(org.elasticsearch.index.engine.VersionConflictEngineException e) {
                      System.out.println("Failed to lock & update content: " + c);
                  }
              }
        }

        return toReturn;
    }


    @Override
    public void markAsDone(T t) {
        t.setStatus(QueueItem.Status.DONE);
        save(t);
    }

    @Override
    public void markAsDone(List<T> list) {
        for(T t : list) {
            markAsDone(t);
        }
    }

    public Class<T> gettClass() {
        return tClass;
    }

    public void settClass(Class<T> tClass) {
        this.tClass = tClass;
    }

    public ElasticsearchTemplate getElasticsearchTemplate() {
        return elasticsearchTemplate;
    }

    public void setElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }
}
