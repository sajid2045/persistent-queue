package au.com.foxsports.commons.jobqueue;

import au.com.foxsports.commons3.json.ISODateTimeNoMillisSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer;
import org.joda.time.DateTime;
import org.springframework.data.annotation.Version;

/**
 * Created by smoinuddin on 22/03/14.
 */
public class QueueItem {
    public enum Status {
        QUEUED ,
        PROCESSING,
        DONE
    }

    String id;
    private Status status;

    private long maxTimeAllowedInProcessing = Long.MAX_VALUE;


    @JsonSerialize(using = JodaDateTimeNoMillisSerializer.class)
    private DateTime created;
    @JsonSerialize(using=DateTimeSerializer.class)
    private DateTime lastModified;


    @Version
    private Long version = 0L;

    public QueueItem() {}

    public QueueItem(String id) {
        this.id = id;
        this.created = DateTime.now();
        this.lastModified = DateTime.now();
        this.status = Status.QUEUED;
    }


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public DateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(DateTime lastModified) {
        this.lastModified = lastModified;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public long getMaxTimeAllowedInProcessing() {
        return maxTimeAllowedInProcessing;
    }

    public void setMaxTimeAllowedInProcessing(long maxTimeAllowedInProcessing) {
        this.maxTimeAllowedInProcessing = maxTimeAllowedInProcessing;
    }
}
